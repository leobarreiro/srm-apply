# SRM Asset Apply Test

### Autor
Leopoldo Braga Barreiro
leopoldo.java@gmail.com
(51) 98549 0948
https://www.linkedin.com/in/leopoldobarreiro/


### Tecnologias utilizadas

Esta POC utiliza as seguintes tecnologias:
- spring-boot com undertow
- spring-data
- jpa com implementação hibernate
- devtools
- spring-actuator
- caffeine-cache
- lombok
- swagger (documentação) 


## Compilando a aplicação

Clonar o projeto, e selecionar o branch develop.

```sh
$ git clone http://gitlab.com/leobarreiro/srm-apply.git
$ git checkout develop
```

Execute a api utilizando uma das opções disponíveis na próxima seção deste README (Eecução).

## Acessando os endpoints

Uma vez executando, a poc está disponível na URL abaixo:

```
http://localhost:8187
```

## Documentação da API

Acesse a documentação da api para obter instruções de uso dos endpoints.

```
http://localhost:8187/swagger-ui.html
```

### Projeto Postman

Está disponível uma coleção de requests do postman para testes da api: _src/postman/_
O arquivo pode ser importado no cliente Postman.


### Banco de dados

Para efeitos de portabilidade, a POC utiliza banco de dados embarcado H2.

Para acessar os dados do banco H2, acesse a URL abaixo:

http://localhost:8187/h2/

Utilize os dados abaixo para acessar o banco:

- Driver Class: org.h2.Driver
- JDBC URL: jdbc:h2:file:~/h2/srmapply
- User Name: srmapply
- Password: srm@123


Após o login, é possível acessar a tabela de análises de risco.


# Execução


### Java JAR

Executando a api via java -jar no ambiente local:

- Compilar a api

```sh
$ mvn clean package -U
```

- Executar com java -jar

```sh
$ java -jar target/srm-apply-0.0.1-SNAPSHOT.jar
```

### Maven

Executando via Maven no ambiente local:

```sh
$ mvn clean spring-boot:run
```

### Docker

Executando via Docker no ambiente local:

```sh
$ mvn clean package
$ cd target
$ docker build -t srm-apply:0.0.1 .
$ docker run -d -p 8187:8187 --name srm-apply srm-apply:0.0.1
```