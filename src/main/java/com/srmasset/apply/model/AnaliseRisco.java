package com.srmasset.apply.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.srmasset.apply.pojo.CategoriaRisco;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "analise_risco")
@JsonIgnoreProperties({"id"})
@SequenceGenerator(name = "analise_seq", sequenceName = "analise_risco_seq", allocationSize = 1, initialValue = 1)
public class AnaliseRisco implements Serializable {

	private static final long serialVersionUID = 2410250576886259428L;

	@Id
	@Column(name = "analise_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "analise_seq")
	private Long id;

	@NotNull(message = "Nome do Cliente não pode ser nulo")
	@Column(name = "nome_cliente", length = 60)
	private String nomeCliente;

	@Column(name = "data_proposta")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dataProposta;

	@NotNull(message = "Valor solicitado não pode ser nulo")
	@Column(name = "valor_solicitado")
	private BigDecimal valorSolicitado;

	@NotNull(message = "Categoria do risco não pode ser nula")
	@Column(name = "categoria_risco", length = 2)
	@Enumerated(EnumType.STRING)
	private CategoriaRisco categoriaRisco;

	@Column(name = "taxa_praticada")
	private BigDecimal taxaPraticada;

	@Column(name = "valor_juros")
	private BigDecimal valorJuros;

	@Column(name = "valor_total")
	private BigDecimal valorTotal;

}
