package com.srmasset.apply.validator;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.srmasset.apply.exception.ApiErrorException;
import com.srmasset.apply.exception.ApiErrorResponse;
import com.srmasset.apply.pojo.AnaliseProposta;
import com.srmasset.apply.pojo.AnaliseRequest;

import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AnaliseRiscoValidator {

	public void accept(AnaliseRequest request) {
		if (ObjectUtils.isEmpty(request)) {
			throw new ApiErrorException(ApiErrorResponse.builder().message("Dados para análise de risco devem estar preenchidos.").build());
		}
		validate(request);
	}

	public void accept(AnaliseProposta proposta) {
		if (ObjectUtils.isEmpty(proposta)) {
			throw new ApiErrorException(ApiErrorResponse.builder().message("Proposta deve estar preenchida.").status(HttpStatus.BAD_REQUEST).build());
		}
		validate(proposta);
	}

	public final void validate(Object req) {
		List<Field> fields = Arrays.asList(req.getClass().getDeclaredFields());
		fields.stream().filter(f -> f.getAnnotation(ApiModelProperty.class) != null).forEach(f -> {
			ApiModelProperty mp = f.getAnnotation(ApiModelProperty.class);
			if (mp.required()) {
				String notes = StringUtils.isNotBlank(mp.notes()) ? mp.notes() : StringUtils.EMPTY;
				if (StringUtils.equals(StringUtils.EMPTY, notes)) {
					notes = StringUtils.isNotBlank(mp.name()) ? mp.name() : null;
				}
				f.setAccessible(Boolean.TRUE);
				verifyField(f, req, notes);
			}
		});
	}

	private final void verifyField(Field f, Object req, String notes) {
		String errorMessage = MessageFormat.format("Atributo obrigatório e não informado <{0}>.", f.getName());
		try {
			if (f.getType().equals(String.class)) {
				String value = (String) f.get(req);
				if (StringUtils.isBlank(value)) {
					throw new ApiErrorException(ApiErrorResponse.builder().status(HttpStatus.BAD_REQUEST).message(errorMessage).build());
				}
			} else if (f.getType().equals(Double.class)) {
				Double value = (Double) f.get(req);
				if (value == null) {
					throw new ApiErrorException(ApiErrorResponse.builder().status(HttpStatus.BAD_REQUEST).message(errorMessage).build());
				}
			} else if (f.getType().isAssignableFrom(List.class)) {
				@SuppressWarnings("unchecked")
				List<Object> value = (List<Object>) f.get(req);
				if (value == null || value.isEmpty()) {
					throw new ApiErrorException(ApiErrorResponse.builder().status(HttpStatus.BAD_REQUEST).message(errorMessage).build());
				}
			} else {
				Object value = f.get(req);
				if (value == null) {
					throw new ApiErrorException(ApiErrorResponse.builder().status(HttpStatus.BAD_REQUEST).message(errorMessage).build());
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ApiErrorException(ApiErrorResponse.builder().message("Erro ao validar Requisição.").build());
		}
	}

}
