package com.srmasset.apply.web;

import org.springframework.http.ResponseEntity;

import com.srmasset.apply.model.AnaliseRisco;
import com.srmasset.apply.pojo.AnaliseProposta;
import com.srmasset.apply.pojo.AnaliseRequest;
import com.srmasset.apply.pojo.AnaliseRiscoHistoricoCliente;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = {"SRM Asset - Análise de Risco"})
public interface ApiSrmApplyInterface {

	@ApiOperation(consumes = "text/json", tags = {"Teste de funcionamento geral."}, value = "Teste Alive Message")
	String getTesting();

	@ApiOperation(consumes = "text/json", tags = {"Proposta de Empréstimo."}, value = "Obtem uma Proposta com base nos dados submetidos")
	ResponseEntity<AnaliseProposta> getAnaliseProposta(AnaliseRequest request);

	@ApiOperation(consumes = "text/json", tags = {"Salva Análise de Risco para Empréstimo."}, value = "Realiza o registro da análise de risco")
	ResponseEntity<AnaliseRisco> salvarAnaliseRisco(AnaliseRequest proposta);

	@ApiOperation(consumes = "text/json", tags = {
		"Histórico de Análises de Risco para o Cliente."}, value = "Obtem uma lista histórica de análises de risco para o cliente")
	ResponseEntity<AnaliseRiscoHistoricoCliente> listarAnalisesCliente(String nomeCliente);

}
