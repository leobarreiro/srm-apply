/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.srmasset.apply.web;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.srmasset.apply.model.AnaliseRisco;
import com.srmasset.apply.pojo.AnaliseProposta;
import com.srmasset.apply.pojo.AnaliseRequest;
import com.srmasset.apply.pojo.AnaliseRiscoHistoricoCliente;
import com.srmasset.apply.service.AnaliseRiscoService;

@RestController
public class ApiSrmApplyController {

	@Autowired
	private AnaliseRiscoService service;

	@GetMapping("/alive")
	@ResponseBody
	public String getTesting() throws IOException {
		return "I am alive";
	}

	@PostMapping("/proposta")
	public ResponseEntity<AnaliseProposta> getAnaliseProposta(@RequestBody AnaliseRequest request) {
		return new ResponseEntity<>(service.calcularRisco(request), HttpStatus.OK);
	}

	@PostMapping("/salvar")
	public ResponseEntity<AnaliseRisco> salvarAnaliseRisco(@RequestBody AnaliseRequest proposta) {
		return new ResponseEntity<>(service.saveAnaliseRisco(proposta), HttpStatus.OK);
	}

	@GetMapping("/listar/{nomeCliente}")
	public ResponseEntity<AnaliseRiscoHistoricoCliente> listarAnalisesCliente(@PathVariable("nomeCliente") String nomeCliente) {
		return new ResponseEntity<>(service.findAnaliseRiscoByNomeCliente(nomeCliente), HttpStatus.OK);
	}

}
