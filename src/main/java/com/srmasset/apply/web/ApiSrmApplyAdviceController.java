package com.srmasset.apply.web;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.srmasset.apply.exception.ApiErrorException;
import com.srmasset.apply.exception.ApiErrorResponse;

@RestControllerAdvice
public class ApiSrmApplyAdviceController {

	@ExceptionHandler(IOException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApiErrorResponse errorIOException(IOException exc) {
		return ApiErrorResponse.builder().message(exc.getMessage()).build();
	}

	@ExceptionHandler(ApiErrorException.class)
	public ResponseEntity<ApiErrorResponse> errorApiException(ApiErrorException exc) {
		return new ResponseEntity<>(exc.getErrorResponse(), exc.getErrorResponse().getStatus());
	}

}
