package com.srmasset.apply.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.srmasset.apply.model.AnaliseRisco;

@Repository
public interface AnaliseRiscoRepository extends JpaRepository<AnaliseRisco, Long> {

	public List<AnaliseRisco> findAnaliseRiscoByNomeClienteOrderByDataPropostaDesc(String nomeCliente);

}
