package com.srmasset.apply.pojo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CategoriaRisco {

	A(0d), B(10d), C(20d);

	private double taxa;

}
