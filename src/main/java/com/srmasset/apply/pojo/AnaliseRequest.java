package com.srmasset.apply.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Requisição para cálculo de Propostas e Persistência de Análises.")
public class AnaliseRequest implements Serializable {

	private static final long serialVersionUID = 2643212834409646821L;

	@ApiModelProperty(notes = "Nome do Cliente", required = true)
	private String nomeCliente;

	@ApiModelProperty(notes = "Categoria de Risco", required = true)
	private CategoriaRisco categoriaRisco;

	@ApiModelProperty(notes = "Valor Monetário Solicitado", required = true)
	private Double valorSolicitado;

}
