package com.srmasset.apply.pojo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.srmasset.apply.model.AnaliseRisco;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnaliseRiscoHistoricoCliente implements Serializable {

	private static final long serialVersionUID = -5226717493695251529L;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Builder.Default
	private LocalDateTime dataRequisicao = LocalDateTime.now();

	private List<AnaliseRisco> analises;

}
