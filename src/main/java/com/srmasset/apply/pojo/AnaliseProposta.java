package com.srmasset.apply.pojo;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AnaliseProposta implements Serializable {

	private static final long serialVersionUID = -4850844347889263879L;

	private String nomeCliente;
	private CategoriaRisco categoriaRisco;
	private Double valorSolicitado;
	private Double valorJuros;
}
