package com.srmasset.apply.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.srmasset.apply.model.AnaliseRisco;
import com.srmasset.apply.repository.AnaliseRiscoRepository;

@Configuration
@EnableJpaRepositories(basePackageClasses = {AnaliseRiscoRepository.class})
@EntityScan(basePackageClasses = {AnaliseRisco.class})
public class JPAConfig {

}
