package com.srmasset.apply.config;

import java.util.Arrays;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.github.benmanes.caffeine.cache.CaffeineSpec;

@EnableCaching
@Configuration
public class CacheConfig extends CachingConfigurerSupport {

	@Bean
	@Override
	public CacheManager cacheManager() {
		final CaffeineCacheManager mgr = new CaffeineCacheManager();
		mgr.setCacheNames(Arrays.asList("calcular-risco", "analise-risco"));
		mgr.setCaffeineSpec(CaffeineSpec.parse("expireAfterWrite=10m"));
		return mgr;
	}

	@Bean
	@Primary
	@Override
	public KeyGenerator keyGenerator() {
		return new CacheKeyGenerator();
	}

}
