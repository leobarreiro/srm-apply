package com.srmasset.apply.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.srmasset.apply.model.AnaliseRisco;
import com.srmasset.apply.pojo.AnaliseProposta;
import com.srmasset.apply.pojo.AnaliseRequest;
import com.srmasset.apply.pojo.AnaliseRiscoHistoricoCliente;
import com.srmasset.apply.repository.AnaliseRiscoRepository;
import com.srmasset.apply.validator.AnaliseRiscoValidator;

@Service
public class AnaliseRiscoService {

	@Autowired
	private AnaliseRiscoRepository repo;

	@Autowired
	private AnaliseRiscoValidator validator;

	@Cacheable(value = "calcular-risco")
	public AnaliseProposta calcularRisco(AnaliseRequest request) {
		validator.accept(request);
		double valorJuros = (request.getValorSolicitado() / 100) * request.getCategoriaRisco().getTaxa();
		return AnaliseProposta.builder()
			.categoriaRisco(request.getCategoriaRisco())
			.nomeCliente(request.getNomeCliente())
			.valorSolicitado(request.getValorSolicitado())
			.valorJuros(valorJuros)
			.build();
	}

	public AnaliseRiscoHistoricoCliente findAnaliseRiscoByNomeCliente(String nomeCliente) {
		return AnaliseRiscoHistoricoCliente.builder().analises(repo.findAnaliseRiscoByNomeClienteOrderByDataPropostaDesc(nomeCliente)).build();
	}

	@Cacheable(value = "analise-risco")
	public AnaliseRisco saveAnaliseRisco(AnaliseRequest request) {
		validator.accept(request);
		double valorJuros = (request.getValorSolicitado() / 100) * request.getCategoriaRisco().getTaxa();
		double valorTotal = request.getValorSolicitado() + valorJuros;
		AnaliseRisco analise = AnaliseRisco.builder()
			.nomeCliente(StringUtils.trim(request.getNomeCliente()))
			.dataProposta(LocalDateTime.now())
			.categoriaRisco(request.getCategoriaRisco())
			.taxaPraticada(BigDecimal.valueOf(request.getCategoriaRisco().getTaxa()).setScale(2, BigDecimal.ROUND_HALF_EVEN))
			.valorJuros(BigDecimal.valueOf(valorJuros).setScale(2, BigDecimal.ROUND_HALF_EVEN))
			.valorSolicitado(BigDecimal.valueOf(request.getValorSolicitado()).setScale(2, BigDecimal.ROUND_HALF_EVEN))
			.valorTotal(BigDecimal.valueOf(valorTotal).setScale(2, BigDecimal.ROUND_HALF_EVEN))
			.build();
		repo.save(analise);
		return analise;
	}

}
