package com.srmasset.apply.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@ResponseBody
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Builder.Default
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime datetime = LocalDateTime.now();

	@Builder.Default
	private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

	@Builder.Default
	private String message = "Erro interno do servidor";

}
