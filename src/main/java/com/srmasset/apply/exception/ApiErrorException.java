package com.srmasset.apply.exception;

import lombok.Getter;

@Getter
public class ApiErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final ApiErrorResponse errorResponse;

	public ApiErrorException() {
		super();
		this.errorResponse = ApiErrorResponse.builder().build();
	}

	public ApiErrorException(ApiErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}

	public ApiErrorException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		this.errorResponse = ApiErrorResponse.builder().message(arg0).build();
	}

	public ApiErrorException(String arg0) {
		super(arg0);
		this.errorResponse = ApiErrorResponse.builder().message(arg0).build();
	}

}
