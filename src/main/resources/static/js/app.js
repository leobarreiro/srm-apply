let app = angular.module('AnaliseManagement', []);

app.controller('AnaliseController', function($scope, $http) {

	$scope.analiseForm = {
		nomeCliente: '',
		categoriaRisco: 'A',
		valorSolicitado: 0.00};

	$scope.analises = [];
	$scope.message = '';
	$scope.response = {};
	$scope.propostaValida = false;

	const config = {headers: {'Content-Type' : 'application/json;'}};

	$scope.calcularProposta = function() {
		_validarProposta();

		if ($scope.propostaValida) {
			$http.post('/proposta', angular.toJson($scope.analiseForm), config).then(
					function(response) {
						$scope.response = response;
						$scope.message = 'Proposta calculada corretamente.';
						$scope.propostaResponse = response.data;
					},
					function(response) {
						$scope.response = response;
						$scope.message = response.data.message;
					}
			);
		}
	};

	$scope.submitProposta = function() {
		$http.post('/salvar', angular.toJson($scope.analiseForm), config).then(
			function(response) {
				$scope.response = response;
				$scope.message = 'Proposta salva corretamente';
				_consultarHistorico();
				_clearFormData();
			},
			function(response) {
				$scope.response = response;
				$scope.message = response.data.message;
			}
		);
	};

	$scope.listarHistorico = function() {
		_consultarHistorico();
	};

	$scope.createProposta = function() {
		_clearFormData();
	};

	function _consultarHistorico() {
		if ($scope.analiseForm.nomeCliente.length == 0) {
			return;
		}
		$http.get('/listar/' + $scope.analiseForm.nomeCliente, config).then(
				function(response) {
					$scope.response = response;
					$scope.analises = response.data.analises;
				},
				function(response) {
					$scope.response = response;
					$scope.message = response.data.message;
					$scope.analises = [];
				}
			);
	}

	function _clearFormData() {
		$scope.analiseForm.nomeCliente = '';
		$scope.analiseForm.categoriaRisco = 'A';
		$scope.analiseForm.valorSolicitado = 0;
		$scope.propostaResponse = null;
	}

	function _validarProposta() {
		$scope.propostaValida = $scope.analiseForm.nomeCliente.length > 0 && $scope.analiseForm.valorSolicitado > 0;
	}

});